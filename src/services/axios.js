import axios from "axios";

// Axios configuration
const token = localStorage.getItem("authtoken");

const instance = axios.create({
  baseURL: "https://steeldor-api-luisoctavio35-gmailcom.vercel.app/api",
});

instance.defaults.headers.common["Access-Control-Allow-Origin"] = "*";

if (token) {
  instance.defaults.headers.common["Authorization"] = token;
}

export default instance;
