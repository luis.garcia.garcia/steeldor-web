import { RouterProvider } from "react-router-dom";
import router from "./router";
import SpinnerOfDoom from "./screens/SpinnerOfDoom";

const App = () => {
  return <RouterProvider fallbackElement={<SpinnerOfDoom />} router={router} />;
};

export default App;
