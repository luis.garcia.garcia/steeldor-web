import { useForm } from "../hooks";
import { Input, Button } from ".";

const SignUpForm = ({ submmit }) => {
  const initialForm = {
    fullname: "",
    email: "",
    password: "",
    passwordConfirm: "",
  };
  const [form, handleChange, reset] = useForm(initialForm);
  const handleSubmmit = (e) => {
    e.preventDefault();
    submmit(form);
    reset();
  };

  return (
    <form onSubmit={handleSubmmit}>
      <Input
        label="Fullname"
        name="fullname"
        value={form.fullname}
        onChange={handleChange}
        required
      />
      <Input
        label="Email"
        name="email"
        value={form.email}
        onChange={handleChange}
        type="email"
        required
      />
      <Input
        label="Password"
        name="password"
        value={form.password}
        onChange={handleChange}
        type="password"
        required
      />
      <Input
        label="Confirm password"
        name="passwordConfirm"
        value={form.passwordConfirm}
        onChange={handleChange}
        type="password"
        required
      />
      <Button> SignUp </Button>
    </form>
  );
};

export default SignUpForm;
