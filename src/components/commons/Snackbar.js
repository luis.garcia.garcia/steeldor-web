import "./Snackbar.css";

export const showSnackbar = () => {
  var snackbar = document.getElementById("snackbar");

  snackbar.classList.add("show");

  setTimeout(function () {
    snackbar.classList.remove("show");
  }, 3000);
};

const Snackbar = ({ children, color = "primary" }) => {
  return (
    <div id="snackbar" className={color}>
      {children}
    </div>
  );
};

export default Snackbar;
