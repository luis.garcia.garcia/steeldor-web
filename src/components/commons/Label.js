const Label = ({
  children,
  fontWeight = 700,
  fontSize = 20,
  color = "#000",
  style,
}) => {
  return (
    <div style={{ ...style, fontWeight, fontSize, color }}>{children}</div>
  );
};

export default Label;
