import "./Spinner.css";

const Spinner = ({ size }) => {
  return <div className="loader" style={{ width: size, height: size }}></div>;
};

export default Spinner;
