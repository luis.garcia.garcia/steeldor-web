import { useForm } from "../hooks";
import { Input, Button } from ".";

const LogInForm = ({ submmit }) => {
  const initialForm = {
    email: "",
    password: "",
  };
  const [form, handleChange, reset] = useForm(initialForm);
  const handleSubmmit = (e) => {
    e.preventDefault();
    submmit(form);
    reset();
  };

  return (
    <form onSubmit={handleSubmmit}>
      <Input
        label="Email"
        name="email"
        value={form.email}
        onChange={handleChange}
        type="email"
        required
      />
      <Input
        label="Password"
        name="password"
        value={form.password}
        onChange={handleChange}
        type="password"
        required
      />
      <Button> LogIn </Button>
    </form>
  );
};

export default LogInForm;
