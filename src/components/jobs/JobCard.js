import "./JobCard.css";
import { Card } from "../../components";

const JobCard = ({ job }) => {
  return (
    <Card style={{ maxWidth: 300, marginTop: 20 }}>
      <card-title> {job.company_name} </card-title>
      <card-body> {job.description} </card-body>
    </Card>
  );
};

export default JobCard;
