import axios from "axios";

const initialState = {
  loading: false,
  user: {},
  error: null,
};

const types = {
  SET_USER: "SET_USER",
  START_REQUEST: "START_REQUEST",
  ERROR_REQUEST: "ERROR_REQUEST",
};

const setUser = (user) => ({ type: types.SET_USER, payload: user });
const startRequest = () => ({ type: types.START_REQUEST });
const setError = (error) => ({ type: types.ERROR_REQUEST, error });

const actions = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_USER:
      return {
        ...state,
        loading: false,
        user: action.payload,
      };
    case types.START_REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case types.ERROR_REQUEST:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    default:
      return state;
  }
};

export default actions;

export const getUser = () => async (dispatch, getState) => {
  dispatch(startRequest());
  axios
    .get("/auth")
    .then((res) => {
      if (res.status === 200) {
        setUser(res.data);
      }
    })
    .catch((e) => {
      console.log(e);
      setError(e);
    });
};
