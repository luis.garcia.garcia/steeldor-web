import { axios } from "../services";

const initialState = {
  list: [],
  error: null,
  loading: false,
};

const types = {
  SET_JOBS: "SET_JOBS",
  START_REQUEST: "START_REQUEST",
  ERROR_REQUEST: "ERROR_REQUEST",
};

const setJobs = (jobs) => ({ type: types.SET_JOBS, payload: jobs });
const startRequest = () => ({ type: types.START_REQUEST });
const setError = (error) => ({ type: types.ERROR_REQUEST, payload: error });

const actions = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_JOBS:
      return {
        ...state,
        list: action.payload,
        loading: false,
      };
    case types.START_REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case types.ERROR_REQUEST:
      return {
        ...state,
        error: action.payload,
        loading: false,
      };
    default:
      return state;
  }
};

export default actions;

export const getJobs = async (dispatch, getState) => {
  dispatch(startRequest());
  axios
    .get("/jobs")
    .then((res) => {
      if (res.status === 200) dispatch(setJobs(res.data));
    })
    .catch((error) => {
      dispatch(setError(error.message));
    });
};
