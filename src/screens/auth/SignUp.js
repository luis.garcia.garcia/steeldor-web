import { Link, useNavigate } from "react-router-dom";
import { Container, Card, SignUpForm, Label } from "../../components";
import Snackbar, { showSnackbar } from "../../components/commons/Snackbar";
import { axios } from "../../services";
import { useState } from "react";

const SignUp = () => {
  const navigate = useNavigate();
  const [color, setColor] = useState("primary");
  const [message, setMessage] = useState("");

  const signUp = async (user) => {
    if (user.password !== user.passwordConfirm) {
      setColor("error");
      setMessage("Passwords doesn't match");
      showSnackbar();
      return;
    }

    axios
      .post("/auth/register", user)
      .then((res) => {
        if (res.status === 200) {
          localStorage.setItem("authtoken", res.data.token);
          axios.defaults.headers.common["Authorization"] = res.data.token;
          navigate("/");
        }
      })
      .catch((err) => {
        if (!!err.response && err.response.status === 502) {
          setMessage(err.response.data);
        } else {
          setMessage(err.message);
        }
        setColor("error");
        showSnackbar();
      });
  };

  return (
    <Container style={{ marginTop: "15%", maxWidth: 600 }}>
      <Card>
        <div style={{ padding: 20 }}>
          <Label style={{ textAlign: "center", marginBottom: 10 }}>
            Sign Up
          </Label>
          <SignUpForm submmit={signUp} />
          <div style={{ textAlign: "center" }}>
            Do you already have an account? <Link to={"/login"}>Log In</Link>
          </div>
        </div>
      </Card>
      <Snackbar color={color}>{message}</Snackbar>
    </Container>
  );
};

export default SignUp;
