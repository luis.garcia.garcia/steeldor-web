import { Link, useNavigate } from "react-router-dom";
import { Container, Card, LogInForm, Label } from "../../components";
import { axios } from "../../services";
import Snackbar, { showSnackbar } from "../../components/commons/Snackbar";
import { useState } from "react";

const LogIn = () => {
  const navigate = useNavigate;
  const [color, setColor] = useState("primary");
  const [message, setMessage] = useState("");

  const submmit = async (credentials) => {
    axios
      .post("/auth/login", credentials)
      .then((res) => {
        if (res.status === 200) {
          localStorage.setItem("authtoken", res.data.token);
          axios.defaults.headers.common["Authorization"] = res.data.token;
          navigate("/");
        } else {
          setColor("error");
          setMessage("Email or password incorrect");
          showSnackbar();
        }
      })
      .catch((error) => {
        setColor("error");
        setMessage("Email or password incorrect");
        showSnackbar();
      });
  };

  return (
    <Container style={{ marginTop: "15%", maxWidth: 600 }}>
      <Card>
        <div style={{ padding: 20 }}>
          <Label style={{ textAlign: "center", marginBottom: 10 }}>
            Log In
          </Label>
          <LogInForm submmit={submmit} />
          <div style={{ textAlign: "center" }}>
            Don't have an account yet? <Link to={"/signup"}>Register now</Link>
          </div>
        </div>
      </Card>
      <Snackbar color={color}>{message}</Snackbar>
    </Container>
  );
};

export default LogIn;
