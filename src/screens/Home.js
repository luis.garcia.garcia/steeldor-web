import { useEffect } from "react";
import { getJobs } from "../reducers/jobs";
import { connect } from "react-redux";
import { SpinnerOfDoom } from "../screens";
import { Container, JobCard } from "../components";

const Home = ({ getJobs, jobs }) => {
  useEffect(() => {
    getJobs();
  }, [getJobs]);

  if (jobs.loading) return <SpinnerOfDoom />;

  return (
    <Container
      style={{
        display: "grid",
        gridTemplateColumns: "repeat(auto-fill, minmax(300px, 1fr)",
        gridAutoRows: "minmax(100px,auto)",
        gripGap: 20,
        padding: 10,
      }}
    >
      {jobs.list.map((item) => (
        <JobCard key={item.id} job={item} />
      ))}
      {jobs.list.map((item) => (
        <JobCard key={item.id} job={item} />
      ))}
      {jobs.list.map((item) => (
        <JobCard key={item.id} job={item} />
      ))}
      {jobs.list.map((item) => (
        <JobCard key={item.id} job={item} />
      ))}
    </Container>
  );
};

const mapStateToProps = (state) => {
  return { jobs: state.jobs };
};

const mapDispatchToProps = (dispatch) => ({
  getJobs: () => dispatch(getJobs),
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
