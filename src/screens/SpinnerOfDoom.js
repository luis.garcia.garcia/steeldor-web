import { Spinner } from "../components";

const SpinnerOfDoom = () => {
  return (
    <div
      style={{
        position: "fixed",
        top: "calc(50% - 80px)",
        left: "calc(50% - 80px)",
      }}
    >
      <Spinner size={80} />
    </div>
  );
};

export default SpinnerOfDoom;
