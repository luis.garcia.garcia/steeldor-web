import { createBrowserRouter } from "react-router-dom";
import { Home, LogIn, SignUp } from "../screens";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Home />,
  },
  {
    path: "/login",
    element: <LogIn />,
  },
  {
    path: "/signup",
    element: <SignUp />,
  },
]);

export default router;
